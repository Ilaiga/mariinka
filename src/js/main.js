"use strict";

//Placeholders for select
$(document).ready(function () {
    $('.__category').select2({
        placeholder: 'Все категории',
        dropdownAutoWidth: true,
    });
    $('.__rooms').select2({
        placeholder: 'Все сцены'
    });

    //Calendar init
    $("#myCalendar").ionCalendar({
        lang: "ru",
        sundayFirst: false,
        format: "DD.MM.YYYY"
    });

    $("#myCalendarPopup").ionCalendar({
        lang: "ru",
        sundayFirst: false,
        format: "DD.MM.YYYY"
    });

    $(".js-search-icon").click(function () {
        $(".js-popup-search").removeClass('hidden-visible');
    });

    $(".js-close").click(function () {
        $(".js-popup-search").addClass('hidden-visible');
    });
});

//Datepicker init
$("#myDatePicker-1").ionDatePicker({
    lang: "ru",
    sundayFirst: false,
    format: "DD.MM.YYYY"
});


//Меняем цвет в зависимости от числа в корзине
let cartCount = parseInt($('.js-cart-count').html());

if (cartCount > 0) {
    $(`.js-cart-count`).addClass('_noempty');
} else {
    $('.js-cart-count').removeClass('_noempty');
}


// sticky switch
let stickySwitch = $('.js-sticky-switch');
if ($(window).width() >= 1200) {

    stickySwitch.sticky({
        zIndex: 20,
        topSpacing: 40,
        bottomSpacing: 740,
        responsiveWidth: true,
        getWidthFrom: '.wrapper',
    });

    stickySwitch.on('sticky-start', function () {
        stickySwitch.addClass('_sticky');
        $('.sticky').addClass('_sticky');
    });
    stickySwitch.on('sticky-end', function () {
        stickySwitch.removeClass('_sticky');
        $('.sticky').removeClass('_sticky');
    })
} else {
    stickySwitch.unstick();
}


$('.switch__icon').on('click', function () {
    let element = $(this).closest('.events');

    $(this).closest('.events__switch').each(function () {
        $(this).find('.switch__icon').removeClass('_active');

    });
    $(this).addClass('_active');

    if ($(this).hasClass('__list _active')) {
        element.addClass('_events-list');
    } else {
        element.removeClass('_events-list');
    }
});


//Подключаем слайдер
$(document).ready(function () {
    $('.js-slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.js-slider-nav'
    });
    $('.js-slider-nav').slick({
        slidesToShow: 2,
        slidesToScroll: 1,

        asNavFor: '.js-slider-for',
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true
    });
    $('.js-slider').slickLightbox({
        lazy: true
    });

    $('.phone-mask').mask('+7 (000) 000 00 00', {placeholder: "+7 (___) ___ __"});
});

//Табы
function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("js-tab");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("js-link");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();


//Показваем комментарий в форме оплаты
$(".js-add-coment").click(function () {
    $(".js-add-coment").addClass('hidden');
    $(this).next(".js-textarea").show();
});

//Скрываем форму оплаты при клике
$(".js-pay-close").click(function () {
    $(".pay").addClass('hidden');
});

//Показываем форму оплаты при клике
$(".js-pay-popup").click(function () {
    $(".pay").removeClass('hidden');
});